---
title: "DamID-piwi_gene_expression"
author: "Sergei Ryazansky"
date: "May 18, 2016"
output: html_document
---


Load data and constant variables
```{r}
library(ggplot2)
if (!require("extrafont")) {
  library('extrafont')
}

if (!require("extrafontdb")) {
  library('extrafontdb')
}

source('../Functions/damid_helper_functions.R')

load('gene_expression/kallisto_expr2.Rda')
load('gene_expression/rsem_expression.Rda')
load('Output_data/DamID_domains_TE.Rda')
load('Output_data/DamID_peaks_TE.Rda')

# low-level cutoff for expression
low_cutoff <- 0.1

lev1 <- c("Promoter (<=1kb)", "Promoter (1-2kb)", "5' UTR", "Exon", 
        "Intron", "3' UTR","Downstream (<1kb)", "Downstream (1-2kb)",
        "Downstream (2-3kb)", "Distal Intergenic", "w/o peaks")

lev2 <- c("Promoter (<=1kb)", "Promoter (1-2kb)", "5' UTR", "Exon", 
          "Intron", "3' UTR", "w/o peaks")

lev3 <- c("Promoter (<=2kb)", "Gene body", "w/o peaks")

# colors of annotion terms
v <- c('Promoter (<=1kb)'='#2d82af', 'Promoter (1-2kb)'='#a6cee3',
       "5' UTR"='#6f9e4c', "3' UTR"='#f16667', 'Exon'='#fe982c',
       "Intron"='#7d64a5', 'w/o peaks'='#b15928')
```

Merging of annotation data w/ expression data
```{r}

# genes
## save data as nested list
expr.data.genes <- list(
    'Sienzki et al. 2015' = list(
        'Kallisto' = sienzki2.genes.expr_kallisto2,
        'RSEM' = sienzki2.genes.expr_rsem),
    'Sienzki et al. 2014' = list(
        'Kallisto' = sienzki.genes.expr_kallisto2,
        'RSEM' = sienzki.genes.expr_rsem),
    'Ohtani et al. 2014' = list(
        'Kallisto' = ohtani.genes.expr_kallisto2,
        'RSEM' = ohtani.genes.expr_rsem),
    'Rozhkov et al. 2013' = list(
        'Kallisto' = rozhkov.genes.expr_kallisto2,
        'RSEM' = rozhkov.genes.expr_rsem)
)

## remove outliers: 5% of highly-expressed genes and all <0.1 low-expressed genes
expr.data.genes <- lapply(expr.data.genes, FUN = function(ref){
    lapply(ref, FUN = function(soft){
        soft <- na.omit(soft)
        up_cutoff <- quantile(soft[,1], probs = 0.95)
        soft <- soft[which(soft[,1] <= up_cutoff),]
        soft <- soft[which(rowSums(soft) > low_cutoff),]
    })
})

########

## merging expression and annotation data
expr.annotated.genes <- data.frame()

for (i in seq(expr.data.genes)) {
    ref_name <- names(expr.data.genes[i])
    expr <- expr.data.genes[[i]]
    result <- data.frame()
    
    for (j in seq(expr)) {
        soft_name = names(expr[j])
        m <- merge_expr_and_annotaion(genes.expr = expr[[j]],
                                      ann_list = damid_genes_annot_domains_woTE,
                                      soft = soft_name,
                                      ref = ref_name,
                                      long_ann = lev1,
                                      brief_ann = lev2)
        result <- rbind(result, m)
    }
    expr.annotated.genes <- rbind(expr.annotated.genes, result)
}

# transcripts
## save data as nested list
expr.data.trs <- list(
    'Sienzki et al. 2015' = list(
        'Kallisto' = sienzki2.tr.expr_kallisto2,
        'RSEM' = sienzki2.tr.expr_rsem),
    'Sienzki et al. 2014' = list(
        'Kallisto' = sienzki.tr.expr_kallisto2,
        'RSEM' = sienzki.tr.expr_rsem),
    'Ohtani et al. 2014' = list(
        'Kallisto' = ohtani.tr.expr_kallisto2,
        'RSEM' = ohtani.tr.expr_rsem),
    'Rozhkov et al. 2013' = list(
        'Kallisto' = rozhkov.tr.expr_kallisto2,
        'RSEM' = rozhkov.tr.expr_rsem)
)

## remove outliers: 5% of highly-expressed genes and all <0.1 low-expressed genes
expr.data.trs <- lapply(expr.data.trs, FUN = function(ref){
    lapply(ref, FUN = function(soft){
        soft <- na.omit(soft)
        up_cutoff <- quantile(soft[,1], probs = 0.95)
        soft <- soft[which(soft[,1] <= up_cutoff),]
        soft <- soft[which(rowSums(soft) > low_cutoff),]
    })
})

## merging expression and annotation data
expr.annotated.trs <- data.frame()

for (i in seq(expr.data.trs)) {
    ref_name <- names(expr.data.trs[i])
    expr <- expr.data.trs[[i]]
    result <- data.frame()
    
    for (j in seq(expr)) {
        soft_name = names(expr[j])
        m <- merge_expr_and_annotaion(genes.expr = expr[[j]],
                                      ann_list = damid_trs_annot_domains_woTE,
                                      soft = soft_name,
                                      ref = ref_name,
                                      long_ann = lev1,
                                      brief_ann = lev2)
        result <- rbind(result, m)
    }
    expr.annotated.trs <- rbind(expr.annotated.trs, result)
}
```

```{r}
save(expr.annotated.genes, expr.annotated.trs, file = 'Output_data/DamID-domains_annotated_expr.Rda')

```

Plotting all Ref and Soft for Piwi-bind genes
```{r}

e.g <- expr.annotated.genes
e.tr <- expr.annotated.trs

bins <- c('91-100%', '81-90%', '71-80%', '61-70%', '51-60%', '41-50%', '31-40%', '21-30%', '11-20%', '1-10%')
levels(e.g$Peaks) <- bins
e.g$Peaks <- ordered(e.g$Peaks, levels=rev(bins))
levels(e.tr$Peaks) <- bins
e.tr$Peaks <- ordered(e.tr$Peaks, levels=rev(bins))

levels(e.g$Phenotype) <- c('GFP-KD', 'Luc-KD', 'Piwi-KD', 'CG9754-KD', 'EGFP-KD', 'HP1-KD', 'shWhite', 'shPiwi')
levels(e.tr$Phenotype) <- c('GFP-KD', 'Luc-KD', 'Piwi-KD', 'CG9754-KD', 'EGFP-KD', 'HP1-KD', 'shWhite', 'shPiwi')

# genes

p <- ggplot(aes(y = log2.TPM, x = Class, fill = Peaks), data = e.g, main='Gene expression')
p <- p + geom_boxplot(outlier.shape=NA, position = position_dodge(width=0.8)) + xlab('') + ylab('Gene expression, log2(TPM)')
#p <- p + scale_fill_manual(values=v)
p <- p + theme_bw() +
    theme(axis.text.x = element_text(family="Arial", size=rel(1.3), angle=30, hjust=1), 
          axis.text.y = element_text(family='Arial'),
          axis.title.y = element_text(family='Arial', size=rel(1.3)),
          legend.text = element_text(family='Arial', size=rel(1.1)),
          legend.title = element_text(family='Arial')
          )
#p <- p + ggtitle("Expression of genes with or w/o Piwi")
p <- p + facet_grid(Soft + Reference ~ Phenotype, scales="free_x")
p
#ggsave(file='Plots/Reanalysis_wTE/Expression_genes_all.pdf', height = 6, width = 12)


p <- ggplot(aes(y = log2.TPM, x = Class, fill = Peaks), data = e.tr, main='Gene expression')
p <- p + geom_boxplot(outlier.shape=NA, position = position_dodge(width=0.8)) + xlab('') + ylab('mRNA expression, log2(TPM)')
#p <- p + scale_fill_manual(values=v)
p <- p + theme_bw() +
    theme(axis.text.x = element_text(family="Arial", size=rel(1.3), angle=30, hjust=1), 
          axis.text.y = element_text(family='Arial'),
          axis.title.y = element_text(family='Arial', size=rel(1.3)),
          legend.text = element_text(family='Arial', size=rel(1.1)),
          legend.title = element_text(family='Arial')
          )
#p <- p + ggtitle("Expression of mRNAs with or w/o Piwi")
p <- p + facet_grid(Soft + Reference ~ Phenotype, scales="free_x")
p
#ggsave(file='Plots/Reanalysis_wTE/Expression_transcripts_all.pdf', height = 6, width = 12)

```

Subset and plotting of data: soft - RSEM, Phenotype-Piwi and Control, Reference -Sienzki2 and Ohtani
```{r}
# filters
subset_filters <- list(soft = 'RSEM',
                       refs = c('Sienzki et al. 2015', 'Ohtani et al. 2014'),
                       phen = c('log2.TPM.gfpKD', 'log2.TPM.egfpKD', 'log2.TPM.piwiKD'),
                       class = c('Promoter (<=1kb)', 'Exon', 'Intron', 'w/o peaks')
                       )

### --> genes
# subsetting
ex.ann.genes.sub <- data_subsetting(expr.annotated.genes, subset_filters)
levels(ex.ann.genes.sub$Phenotype) <- c('GFP-KD', 'Piwi-KD', 'EGFP-KD')
levels(ex.ann.genes.sub$Peaks) <- bins
ex.ann.genes.sub$Peaks <- ordered(ex.ann.genes.sub$Peaks, levels=rev(bins))

ex.ann.genes.sub$Class <- ordered(ex.ann.genes.sub$Class, levels = c('Promoter (<=1kb)', 'Exon', 'Intron', 'w/o peaks'))

# plotting of subset
p <- ggplot(aes(y = log2.TPM, x = Class, fill = Peaks), data = ex.ann.genes.sub, main='Gene expression')
p <- p + geom_boxplot(outlier.size=0, position = position_dodge(width=0.8)) + xlab('') + ylab('Gene expression, log2(TPM)')
#p <- p + scale_fill_manual(values=v)
p <- p + theme_bw() +
    theme(axis.text.x = element_text(family="Arial", size=rel(1.3)), 
          axis.text.y = element_text(family='Arial'),
          axis.title.y = element_text(family='Arial', size=rel(1.3)),
          legend.text = element_text(family='Arial', size=rel(1.1)),
          legend.title = element_text(family='Arial')
          )
#p <- p + ggtitle("Expression of genes with or w/o Piwi")
p <- p + facet_wrap( ~ Reference + Phenotype, scales="free_x")
p
#ggsave(file='Plots/Reanalysis_wTE/Expression_genes_subset.pdf', height = 3, width = 6)

### --> mRNAs
# subsetting
ex.ann.trs.sub <- data_subsetting(expr.annotated.trs, subset_filters)
levels(ex.ann.trs.sub$Phenotype) <- c('GFP-KD', 'EGFP-KD', 'Piwi-KD')
levels(ex.ann.trs.sub$Peaks) <- bins
ex.ann.trs.sub$Peaks <- ordered(ex.ann.trs.sub$Peaks, levels=rev(bins))

# plotting of subset
p <- ggplot(aes(y = log2.TPM, x = Class, fill = Peaks), data = ex.ann.trs.sub, main='mRNA expression')
p <- p + geom_boxplot(outlier.size=0, position = position_dodge(width=0.8)) + xlab('') + ylab('mRNA expression, log2(TPM)')
#p <- p + scale_fill_manual(values=v)
p <- p + theme_bw() +
    theme(axis.text.x = element_text(family="Arial", size=rel(1.3)), 
          axis.text.y = element_text(family='Arial'),
          axis.title.y = element_text(family='Arial', size=rel(1.3)),
          legend.text = element_text(family='Arial', size=rel(1.1)),
          legend.title = element_text(family='Arial')
    )
#p <- p + ggtitle("Expression of mRNAs with or w/o Piwi")
p <- p + facet_grid(Phenotype ~ Reference, scales="free_x")
p
#ggsave(file='Plots/Reanalysis_wTE/Expression_transcripts_subset.pdf', height = 3, width = 6)

```

Testing of significance: Promoter, exon, intron vs w/o peaks
```{r}

## genes
pvals_genes <- vector('list', length=4)
names(pvals_genes) <- names(expr.data.genes)

for (i in seq(pvals_genes)) {
    ref_name <- names(pvals_genes[i])
    
    for (soft_name in c('Kallisto', 'RSEM')) {
        data <- data_subsetting(expr.annotated.genes, 
                                list(soft=soft_name,
                                     refs=ref_name))
        pvs <- list()
        for (ph in unique(data$Phenotype)) {
            res <- data_subsetting(data, list(phen=c(ph)))
            
            pvsr <- list()
            for (bin in unique(data$Peaks)) {
                resf <- data_subsetting(res, list(pk=c(bin)))
                pvsr[[bin]] <- pvalues_wo_peaks(resf)
            }
            
            pvs[[ph]] <- pvsr
        }
        pvals_genes[[ref_name]][[soft_name]] <- pvs
    }
}

## mRNAs
pvals_trs <- vector('list', length=4)
names(pvals_trs) <- names(expr.data.trs)

for (i in seq(pvals_trs)) {
    ref_name <- names(pvals_trs[i])
    
    for (soft_name in c('Kallisto', 'RSEM')) {
        data <- data_subsetting(expr.annotated.trs, 
                                list(soft=soft_name,
                                     refs=ref_name))
        pvs <- list()
        for (ph in unique(data$Phenotype)) {
            res <- data_subsetting(data, list(phen=c(ph)))
            
            pvsr <- list()
            for (bin in unique(data$Peaks)) {
                resf <- data_subsetting(res, list(pk=c(bin)))
                pvsr[[bin]] <- pvalues_wo_peaks(resf)
            }
            
            pvs[[ph]] <- pvsr
        }
        pvals_trs[[ref_name]][[soft_name]] <- pvs
    }
}

```



- associated with TEs?