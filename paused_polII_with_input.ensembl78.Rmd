---
title: "paused_polII_with_input.ensembl78"
author: "Sergei Ryazansky"
date: '7 октября 2016 г '
output: html_document
---

```{r}
#source("https://bioconductor.org/biocLite.R")
#biocLite("compEpiTools")

```


```{r}
library(compEpiTools)

```

Create txdb object from biomart ensembl78
```{r}
library("biomaRt")
# TxDb from Ensemble 78 release of D. melanogaster (dm3)
library("GenomicFeatures")
ens78_dm3_txdb <- makeTxDbFromBiomart("ENSEMBL_MART_ENSEMBL", dataset = "dmelanogaster_gene_ensembl", host="dec2014.archive.ensembl.org")
ens78_dm3_txdb <- dropSeqlevels(ens78_dm3_txdb, c('4', '2LHet', '2RHet', '3LHet', '3RHet', 'XHet', 'YHet', 'Uextra', 'U', 'dmel_mitochondrion_genome'))

seqlevelsStyle(ens78_dm3_txdb) <- 'UCSC'

```

BAMs for stalling index
```{r}
#path <- c('/home/hogart/Data/Clouds/MEGA/Sequencies/BAMs/polII')
#path <- c('/home/hogart/work/OSC_11.14/3_mapped_fastq')
path <- c('/home/hogart/Downloads/polII')

polII_controlKD_bams <- list(paste(path, 'chipseq.polII.EGFP-KD.rep1.sienzki2012.dedup.bam', sep='/'), paste(path, 'chipseq.polII.EGFP-KD.rep2.sienzki2012.dedup.bam', sep='/'))
input_controlKD_bams <- list(paste(path, 'chipseq.input.EGFP-KD.rep1.sienzki2012.dedup.bam', sep='/'), paste(path, 'chipseq.input.EGFP-KD.rep2.sienzki2012.dedup.bam', sep='/'))

polII_piwiKD_bams <- list(paste(path, 'chipseq.polII.piwi-KD.rep1.sienzki2012.dedup.bam', sep='/'), paste(path, 'chipseq.polII.piwi-KD.rep2.sienzki2012.dedup.bam', sep='/'))
input_piwiKD_bams <- list(paste(path, 'chipseq.input.piwi-KD.rep1.sienzki2012.dedup.bam', sep='/'), paste(path, 'chipseq.input.piwi-KD.rep2.sienzki2012.dedup.bam', sep='/'))

# peak file
controlKD_peaks_1 <- read.table(paste(path, 'EGFP-KD.rep1.10e5_peaks.narrowPeak', sep='/'))
controlKD_peaks_2 <- read.table(paste(path, 'EGFP-KD.rep2.10e5_peaks.narrowPeak', sep='/'))
piwiKD_peaks_1 <- read.table(paste(path, 'piwi-KD.rep1.10e5_peaks.narrowPeak', sep='/'))
piwiKD_peaks_2 <- read.table(paste(path, 'piwi-KD.rep2.10e5_peaks.narrowPeak', sep='/'))

controlKD_peaks <- list(cntrKD_1=controlKD_peaks_1, cntrKD_2=controlKD_peaks_2)
piwiKD_peaks <- list(piwiKD_1 = piwiKD_peaks_1, piwiKD_2 = piwiKD_peaks_2)


controlKD_peaks <- lapply(controlKD_peaks, FUN = function(x) {
    y <- GRanges(
    seqnames = Rle(as.character(x$V1)),
    ranges = IRanges(
        start = x$V2,
        end = x$V3),
    strand = Rle(strand(rep('*', nrow(x)))),
    EnrichmentScore=x$V10)
    y <- dropSeqlevels(y, c('chr4', 'chr2LHet', 'chr2RHet', 'chr3LHet', 'chr3RHet', 'chrXHet', 'chrUextra', 'chrU'))
#    seqlevelsStyle(y) <- 'NCBI'
    y
})


piwiKD_peaks <- lapply(piwiKD_peaks, FUN = function(x) {
    y <- GRanges(
    seqnames = Rle(as.character(x$V1)),
    ranges = IRanges(
        start = x$V2,
        end = x$V3),
    strand = Rle(strand(rep('*', nrow(x)))),
    EnrichmentScore=x$V10)
    y <- dropSeqlevels(y, c('chr4', 'chr2LHet', 'chr2RHet', 'chr3LHet', 'chr3RHet', 'chrXHet', 'chrUextra', 'chrU'))
 #   seqlevelsStyle(y) <- 'NCBI'
    y
})

load('Output_data/Fbtr_list_ensembl78.Rda')

# size of libs
libSize <- read.table('Paused_polII/polII_bam_size.txt')
libSizeControlInputList <- list('rep1' = libSize[1,2], 'rep2' = libSize[2,2])
libSizeControlPolIIList <- list('rep1' = libSize[5,2], 'rep2' = libSize[6,2])
libSizePiwiInputList <- list('rep1' = libSize[3,2], 'rep2' = libSize[4,2])
libSizePiwiPolIIList <- list('rep1' = libSize[7,2], 'rep2' = libSize[8,2])

```


```{r}
pi_control <- stallingIndex(BAMlist = polII_controlKD_bams, inputList=input_controlKD_bams, peakGRlist = controlKD_peaks, peakGB=FALSE, transcriptDB = ens78_dm3_txdb, genesList = list( as.character(df_genes$fbgn),as.character(df_genes$fbgn)), countMode="gene", upstream=300,  downstream=400, cutoff=600, elongationOffset=600)

pi_piwi <- stallingIndex(BAMlist = polII_piwiKD_bams, inputList=input_piwiKD_bams, peakGRlist = piwiKD_peaks, peakGB=FALSE, transcriptDB = ens78_dm3_txdb, genesList = list( as.character(df_genes$fbgn),as.character(df_genes$fbgn)), countMode="gene", upstream=300,  downstream=400, cutoff=600, elongationOffset=600)

save(pi_control, pi_piwi, file = 'Output_data/pausingIndex_genes.compEpiTools.Rda')

```

```{r}
source('../Functions/stallingIndex.R')
load('Output_data/Fbtr_list_ensembl78.Rda')

### very long run !!
# simple substraction
si_controlKD <- stalling_index_bins(BAMlist = polII_controlKD_bams, inputList = input_controlKD_bams, upstream = 300 ,downstream = 400, cutoff = 600, elongationOffset = 600, libSizeTrtList=libSizeControlPolIIList, libSizeInputList = libSizeControlInputList, run_type = 'single', binSize = 100, transcriptDB = ens78_dm3_txdb, peakGB = FALSE, peakGRlist = controlKD_peaks, genesList = list(as.character(df_genes$fbgn)), countMode = 'gene')
    
si_piwiKD <- stalling_index_bins(BAMlist = polII_piwiKD_bams, inputList = input_piwiKD_bams, upstream = 300 ,downstream = 400, cutoff = 600, elongationOffset = 600, libSizeTrtList=libSizePiwiPolIIList, libSizeInputList = libSizePiwiInputList, run_type = 'single', binSize = 100, transcriptDB = ens78_dm3_txdb, peakGB = FALSE, peakGRlist = piwiKD_peaks, genesList = list(as.character(df_genes$fbgn)), countMode = 'gene')

si_controlKD <- lapply(si_controlKD, unique)
si_piwiKD <- lapply(si_piwiKD, unique)

save(si_controlKD, si_piwiKD, file = 'Output_data/pausingIndex_genes.Bins.Rda')
 
# the problem with substration is that a lot of genes (> 3k) have negative chip-input signal, so SI also negative
# with log2ratio of chip/input

si_controlKD_l2r <- stalling_index_bins(BAMlist = polII_controlKD_bams, inputList = input_controlKD_bams, upstream = 300 ,downstream = 400, cutoff = 600, elongationOffset = 600, libSizeTrtList=libSizeControlPolIIList, libSizeInputList = libSizeControlInputList, run_type = 'single', binSize = 100, transcriptDB = ens78_dm3_txdb, peakGB = FALSE, peakGRlist = controlKD_peaks, genesList = list(as.character(df_genes$fbgn))[1:100], countMode = 'gene', do_calc = 'log2ratio')

    
si_piwiKD_l2r <- stalling_index_bins(BAMlist = polII_piwiKD_bams, inputList = input_piwiKD_bams, upstream = 300 ,downstream = 400, cutoff = 600, elongationOffset = 600, libSizeTrtList=libSizePiwiPolIIList, libSizeInputList = libSizePiwiInputList, run_type = 'single', binSize = 100, transcriptDB = ens78_dm3_txdb, peakGB = FALSE, peakGRlist = piwiKD_peaks, genesList = list(as.character(df_genes$fbgn)), countMode = 'gene', do_calc = 'log2ratio')

    
```

```{r}
a <- lapply(si_controlKD, as.data.frame)
head(a[[1]][a[[1]]$SI < 0,])

```

