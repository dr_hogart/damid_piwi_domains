---
title: "Correlation of paused pol II and sRNA in promoters"
author: "Sergei Ryazansky"
date: "December 11, 2015"
output: html_document
---

Reads of small RNAs from Piwi-IP from OSC. Bam file is merge of SRR2034810, SRR2034811, SRR2054287 (Ishizu 2015, Homolka 2015). Reads corresponded to Repeats were removed.

```{r}
library(GenomicAlignments)
sRNA_osc <- as(readGAlignments(pate0(ip_dir,"Piwi-IP/piwiip.osc.uniq.remTE.bam")), "GRanges")
save(sRNA_osc, file = "Paused_polII/Loaded_reads_sRNA_OSC_piwiIP.Rda")

```

Number of reads in promoters of transcripts
```{r}
library(TxDb.Dmelanogaster.UCSC.dm3.ensGene)
kgdb <- TxDb.Dmelanogaster.UCSC.dm3.ensGene
kg <- transcripts(kgdb, columns=c("tx_id", "tx_name", "gene_id"))
kg_promoters_UP <- promoters(kg, upstream = 400, down=0)
kg_promoters_DOWN <- promoters(kg, upstream = 0, down=300)

kg_promoters_UP_df <- as.data.frame(kg_promoters_UP)[,c(7,1,2,3,5)]
colnames(kg_promoters_UP_df) <- c("GeneID", "Chr", "Start", "End", "Strand")
kg_promoters_DOWN_df <- as.data.frame(kg_promoters_DOWN)[,c(7,1,2,3,5)]
colnames(kg_promoters_DOWN_df) <- c("GeneID", "Chr", "Start", "End", "Strand")

#load("Paused_polII/Loaded_reads_sRNA_OSC_piwiIP.Rda")
#sRNA_promoters <- summarizeOverlaps(kg_promoters, sRNA_osc)

# ?? strand
#sRNA_promoters <- data.frame('FBtr'=as.data.frame(kg_promoters)$tx_name, 'Reads'=assay(sRNA_promoters)[,1])

# alternative
library(Rsubread)
sRNA_promoters_S_UP <- featureCounts(files=pate0(ip_dir,'Piwi-IP/piwiip.osc.uniq.remTE.bam'), annot.ext=kg_promoters_UP_df, strandSpecific=1)

sRNA_promoters_AS_UP <- featureCounts(files=pate0(ip_dir,'Piwi-IP/piwiip.osc.uniq.remTE.bam'), annot.ext=kg_promoters_UP_df, strandSpecific=2)

sRNA_promoters_S_DOWN <- featureCounts(files=pate0(ip_dir,'Piwi-IP/piwiip.osc.uniq.remTE.bam'), annot.ext=kg_promoters_DOWN_df, strandSpecific=1)

sRNA_promoters_AS_DOWN <- featureCounts(files=pate0(ip_dir,'Piwi-IP/piwiip.osc.uniq.remTE.bam'), annot.ext=kg_promoters_DOWN_df, strandSpecific=2)

sRNA_promoters <- data.frame('FBtr'=rownames(sRNA_promoters_AS_DOWN$counts), 'Sense.UP'=sRNA_promoters_S_UP$counts[,1], 'Antisense.UP'=sRNA_promoters_AS_UP$counts[,1], 'Sense.DOWN'=sRNA_promoters_S_DOWN$counts[,1], 'Antisense.DOWN'=sRNA_promoters_AS_DOWN$counts[,1])

```


```{r}
load("Paused_polII/Paused_indexes.Rda")
p <- 0.01

groseq_shw_pi <- groseq_shw_pi[groseq_shw_pi$Fisher<=p,]
groseq_shp_pi <- groseq_shp_pi[groseq_shp_pi$Fisher<=p,]
polII_KD_pi <- polII_KD_pi[polII_KD_pi$Fisher<=p,]
polII_K_pi <- polII_K_pi[polII_K_pi$Fisher<=p,]


load("Output_data/DamID_peaks.Rda")

pi <- list('GRO-seq: shWhite'=groseq_shw_pi, 'GRO-seq: shPiwi'=groseq_shp_pi, 'PolII: Control'=polII_K_pi, 'PolII: KD Piwi'=polII_KD_pi)

lev <- c("Promoter (<=1kb)", "Promoter (1-2kb)", "Gene body", "w/o peaks")

# Creating the list of annotated transcripts
# and merging annotateted transcritps with transcripts paising index
# and with number of reads in promoter region from piwi-ip data

pi_annotated <- data.frame()

for (i in seq(pi)) {
    df_pi <- pi[[i]][,c(2,3)]
    df_pi <- merge(df_pi, sRNA_promoters, by.x='FBtr', by.y='FBtr')
    q95 <- quantile(df_pi$Sense.UP + df_pi$Antisense.UP + df_pi$Sense.DOWN + df_pi$Antisense.DOWN, probs=c(0.05, 0.95))
    df_pi <- subset(df_pi, Sense.UP + Antisense.UP + Sense.DOWN + Antisense.DOWN < q95[2])
    df_pi <- subset(df_pi, Sense.UP + Antisense.UP + Sense.DOWN + Antisense.DOWN > q95[1])
#    df_pi <- subset(df_pi, Sense + Antisense > 5)
    df_pi$Sense.UP <- log2(df_pi$Sense.UP+1)
    df_pi$Antisense.UP <- log2(df_pi$Antisense.UP+1)
    df_pi$Sense.DOWN <- log2(df_pi$Sense.DOWN+1)
    df_pi$Antisense.DOWN <- log2(df_pi$Antisense.DOWN+1)
    n_genes <- dim(df_pi)[1]
    groups <- list('piwi'=vector(length=n_genes), 'hp1'=vector(length=n_genes))
    
    # 1 for Gene body
    # 9 and 10 for Promoter
    # 0 for w/o peaks
    
    for (d in c('piwi', 'hp1')) {
        for (j in 1:8) {
            groups[[d]][which(df_pi$FBtr %in% cls_tr[[d]][[j]])] <- 1
                }
        for (j in 9:10) {
            groups[[d]][which(df_pi$FBtr %in% cls_tr[[d]][[j]])] <- j
        }
        groups[[d]] <- ordered(groups[[d]], labels=rev(lev))
        groups[[d]] <- ordered(groups[[d]], levels=lev)
    }
    
    df_pi <- data.frame(df_pi, 'PIWI'=groups$piwi, 'HP1'=groups$hp1, 'Experiment'=rep(names(pi[i]), n_genes))
    pi_annotated <- rbind(pi_annotated, df_pi)
    
}

# number of genes
tapply(pi_annotated$PIWI, pi_annotated$Experiment, table)
#$`GRO-seq: shWhite`
#Promoter (<=1kb) Promoter (1-2kb)        Gene body        w/o peaks 
#             568               99              197             3419 

#$`GRO-seq: shPiwi`
#Promoter (<=1kb) Promoter (1-2kb)        Gene body        w/o peaks 
#             583              112              219             3800 

#$`PolII: Control`
#Promoter (<=1kb) Promoter (1-2kb)        Gene body        w/o peaks 
#             483               82              145             3350 

#$`PolII: KD Piwi`
#Promoter (<=1kb) Promoter (1-2kb)        Gene body        w/o peaks 
#             583              112              219             3800
```


```{r}
library(plyr)
library(ggplot2)
library('extrafont')
library('extrafontdb')

v <- c('Promoter (<=1kb)'='#2d82af', 'Promoter (1-2kb)'='#a6cee3', 'Gene body'='#7d64a5', 'w/o peaks'='#b15928')
vs <- c('Promoter (<=1kb)'='#2d82af', 'Gene body'='#7d64a5', 'w/o peaks'='#b15928')

#### PIWI
#pi_annotated_b <- pi_annotated
pi_annotated_b <- subset(pi_annotated, PIWI!='Promoter (1-2kb)')

pi_annotated_b$PauseBins <- cut(pi_annotated_b$Pause, breaks = c(0, 1.5, Inf))
pi_annotated_b$PauseBins <- ordered(pi_annotated_b$PauseBins, labels=c('<1.5', '>1.5' ))

# alternative binning according to quantiles in each experiment
#pi_annotated_b$PauseBins <- unlist(
#    tapply(pi_annotated_b$Pause, pi_annotated_b$Experiment, FUN=function(x) {
#        bins <- cut(x, c(0, quantile(x)[2:4], Inf))
#        ordered(bins, labels=c('<q1', 'q1-q2', 'q2-q3', '>q3' ))}))

pi_annotated_b <- na.omit(pi_annotated_b)

pdf("Plots/Pausing/piwi_smallRNA_Antisense_UP.pdf", height = 5, width = 7, family = 'Arial')
p <- ggplot(pi_annotated_b, aes(x = PauseBins, y = Antisense.UP, fill = PIWI))
#    geom_point(aes(group = PIWI, colour = PIWI))
p <- p + geom_boxplot(outlier.size=0)
p <- p + scale_fill_manual(values=v)
p <- p + facet_wrap(~Experiment, scales="free_x")
#p <- p + scale_y_continuous(limits=c(-1, 6))

p <- p + theme_bw() + theme(axis.text.x = element_text(family="Arial", size=rel(1.3)), 
                            axis.text.y = element_text(family='Arial'),
                            axis.title.y = element_text(family='Arial', size=rel(1.3)),
                            legend.text = element_text(family='Arial', size=rel(1.1)),
                            legend.title = element_text(family='Arial')
)
p <- p + xlab('Pausing index') + ylab('Reads of sRNA from promoters in Piwi-IP, log2')
p
dev.off()

pdf("Plots/Pausing/piwi_smallRNA_Sense_UP.pdf", height = 5, width = 7, family = 'Arial')
p <- ggplot(pi_annotated_b, aes(x = PauseBins, y = Sense.UP, fill = PIWI))
#    geom_point(aes(group = PIWI, colour = PIWI))
p <- p + geom_boxplot(outlier.size=0)
p <- p + scale_fill_manual(values=v)
p <- p + facet_wrap(~Experiment, scales="free_x")
#p <- p + scale_y_continuous(limits=c(-1, 6))

p <- p + theme_bw() + theme(axis.text.x = element_text(family="Arial", size=rel(1.3)), 
                            axis.text.y = element_text(family='Arial'),
                            axis.title.y = element_text(family='Arial', size=rel(1.3)),
                            legend.text = element_text(family='Arial', size=rel(1.1)),
                            legend.title = element_text(family='Arial')
)
p <- p + xlab('Pausing index') + ylab('Reads of sRNA from promoters in Piwi-IP, log2')
p
dev.off()

# So, genes with piwi in promoters and having high paising index tend to have more sRNAs from piwi-ip. Is this because the amoint of genes with piwi larger than genes w/o piwi (so, the first group have more reads than the second group)

pdf("Plots/Pausing/piwi_smallRNA_Antisense_DOWN.pdf", height = 5, width = 7, family = 'Arial')
p <- ggplot(pi_annotated_b, aes(x = PauseBins, y = Antisense.DOWN, fill = PIWI))
#    geom_point(aes(group = PIWI, colour = PIWI))
p <- p + geom_boxplot(outlier.size=0)
p <- p + scale_fill_manual(values=v)
p <- p + facet_wrap(~Experiment, scales="free_x")
#p <- p + scale_y_continuous(limits=c(-1, 6))

p <- p + theme_bw() + theme(axis.text.x = element_text(family="Arial", size=rel(1.3)), 
                            axis.text.y = element_text(family='Arial'),
                            axis.title.y = element_text(family='Arial', size=rel(1.3)),
                            legend.text = element_text(family='Arial', size=rel(1.1)),
                            legend.title = element_text(family='Arial')
)
p <- p + xlab('Pausing index') + ylab('Reads of sRNA from promoters in Piwi-IP, log2')
p
dev.off()

pdf("Plots/Pausing/piwi_smallRNA_Sense_DOWN.pdf", height = 5, width = 7, family = 'Arial')
p <- ggplot(pi_annotated_b, aes(x = PauseBins, y = Sense.DOWN, fill = PIWI))
#    geom_point(aes(group = PIWI, colour = PIWI))
p <- p + geom_boxplot(outlier.size=0)
p <- p + scale_fill_manual(values=v)
p <- p + facet_wrap(~Experiment, scales="free_x")
#p <- p + scale_y_continuous(limits=c(-1, 6))

p <- p + theme_bw() + theme(axis.text.x = element_text(family="Arial", size=rel(1.3)), 
                            axis.text.y = element_text(family='Arial'),
                            axis.title.y = element_text(family='Arial', size=rel(1.3)),
                            legend.text = element_text(family='Arial', size=rel(1.1)),
                            legend.title = element_text(family='Arial')
)
p <- p + xlab('Pausing index') + ylab('Reads of sRNA from promoters in Piwi-IP, log2')
p
dev.off()

```



