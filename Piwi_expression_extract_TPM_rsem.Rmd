---
title: "rsem_expression_analysis"
author: "Sergei Ryazansky"
date: '12 мая 2016 г '
output: html_document
---

```{r}
load('Output_data/Fbtr_list.Rda')

source('../Functions/rsem_functions.R')
source('../Functions/de_functions.R')
```


```{r}
untar(tarfile = '/home/hogart/Data/Clouds/MEGA/Sequencies/GeneExpression/OSC_Expression/75_RSEM_cleaned.tgz', list = FALSE, exdir = "Temp", compressed = 'gzip')
expr_dir='Temp/75_RSEM_cleaned/igenome/ensembl'
sample_id <- dir(file.path(expr_dir))
keep <- grep('genes', sample_id)
sample_id <- strsplit2(sample_id[keep], split = '.genes')[,1]

```


Analysis of RSEM data from Rozhkov et al 2013
```{r}
ids <- sample_id[11:14]

#genes
rozhkov.genes.expr_rsem <- fetch_rsem_expression(sample_ids = ids, expr_folder = expr_dir, type='genes')
fbgns <- rozhkov.genes.expr_rsem[[1]]$gene_id

rozhkov.genes.expr_rsem <- sapply(rozhkov.genes.expr_rsem, FUN=function(x) log2(x$TPM+1))
rozhkov.genes.expr_rsem <- rep_collapsing(rozhkov.genes.expr_rsem)
rownames(rozhkov.genes.expr_rsem) <- fbgns    
rozhkov.genes.expr_rsem <- rozhkov.genes.expr_rsem[,c(2,1)]
colnames(rozhkov.genes.expr_rsem) <- c('log2.TPM.shWhite', 'log2.TPM.shPiwi')
rozhkov.genes.expr_rsem <- as.data.frame(rozhkov.genes.expr_rsem)

# transcripts
rozhkov.tr.expr_rsem <- fetch_rsem_expression(sample_ids = ids, expr_folder = expr_dir, type='isoforms')
fbtrs <- rozhkov.tr.expr_rsem[[1]]$transcript_id

rozhkov.tr.expr_rsem <- sapply(rozhkov.tr.expr_rsem, FUN=function(x) log2(x$TPM+1))
rozhkov.tr.expr_rsem <- rep_collapsing(rozhkov.tr.expr_rsem)
rownames(rozhkov.tr.expr_rsem) <- fbtrs
rozhkov.tr.expr_rsem <- rozhkov.tr.expr_rsem[,c(2,1)]
colnames(rozhkov.tr.expr_rsem) <- c('log2.TPM.shWhite', 'log2.TPM.shPiwi')
rozhkov.tr.expr_rsem <- as.data.frame(rozhkov.tr.expr_rsem)
```

Analysis of RSEM data from Ohtani et al 2014
```{r}
ids <- sample_id[c(1,9,6)]

# genes
ohtani.genes.expr_rsem <- fetch_rsem_expression(sample_ids = ids, expr_folder = expr_dir, type='genes')
fbgns <- ohtani.genes.expr_rsem[[1]]$gene_id

ohtani.genes.expr_rsem <- sapply(ohtani.genes.expr_rsem, FUN=function(x) log2(x$TPM+1))
rownames(ohtani.genes.expr_rsem) <- fbgns    
colnames(ohtani.genes.expr_rsem) <- c('log2.TPM.egfpKD', 'log2.TPM.piwiKD', 'log2.TPM.hp1KD')
ohtani.genes.expr_rsem <- as.data.frame(ohtani.genes.expr_rsem)

# transcripts
ohtani.tr.expr_rsem <- fetch_rsem_expression(sample_ids = ids, expr_folder = expr_dir, type='isoforms')
fbtrs <- ohtani.tr.expr_rsem[[1]]$transcript_id

ohtani.tr.expr_rsem <- sapply(ohtani.tr.expr_rsem, FUN=function(x) log2(x$TPM+1))
rownames(ohtani.tr.expr_rsem) <- fbtrs
colnames(ohtani.tr.expr_rsem) <- c('log2.TPM.egfpKD', 'log2.TPM.piwiKD', 'log2.TPM.hp1KD')
ohtani.tr.expr_rsem <- as.data.frame(ohtani.tr.expr_rsem)
```

Analysis of RSEM data from Sienzke et al 2014
```{r}
ids <- sample_id[c(4,8)]

# genes
sienzki.genes.expr_rsem <- fetch_rsem_expression(sample_ids = ids, expr_folder = expr_dir, type='genes')
fbgns <- sienzki.genes.expr_rsem[[1]]$gene_id

sienzki.genes.expr_rsem <- sapply(sienzki.genes.expr_rsem, FUN=function(x) log2(x$TPM+1))
rownames(sienzki.genes.expr_rsem) <- fbgns    
colnames(sienzki.genes.expr_rsem) <- c('log2.TPM.gfpKD', 'log2.TPM.piwiKD')
sienzki.genes.expr_rsem <- as.data.frame(sienzki.genes.expr_rsem)

# transcripts
sienzki.tr.expr_rsem <- fetch_rsem_expression(sample_ids = ids, expr_folder = expr_dir, type='isoforms')
fbtrs <- sienzki.tr.expr_rsem[[1]]$transcript_id

sienzki.tr.expr_rsem <- sapply(sienzki.tr.expr_rsem, FUN=function(x) log2(x$TPM+1))
rownames(sienzki.tr.expr_rsem) <- fbtrs
colnames(sienzki.tr.expr_rsem) <- c('log2.TPM.gfpKD', 'log2.TPM.piwiKD')
sienzki.tr.expr_rsem <- as.data.frame(sienzki.tr.expr_rsem)
```

Analysis of RSEM data from Sienzke et al 2015
```{r}
ids <- sample_id[c(2,3,5,7,10)]

# genes
sienzki2.genes.expr_rsem <- fetch_rsem_expression(sample_ids = ids, expr_folder = expr_dir, type='genes')
fbgns <- sienzki2.genes.expr_rsem[[1]]$gene_id

sienzki2.genes.expr_rsem <- sapply(sienzki2.genes.expr_rsem, FUN=function(x) log2(x$TPM+1))
sienzki2.genes.expr_rsem <- data.frame(sienzki2.genes.expr_rsem[,c(3:5)], rep_collapsing(sienzki2.genes.expr_rsem[,c(1:2)]))
rownames(sienzki2.genes.expr_rsem) <- fbgns
colnames(sienzki2.genes.expr_rsem) <- c('log2.TPM.gfpKD', 'log2.TPM.lucKD', 'log2.TPM.piwiKD', 'log2.TPM.CG9754KD')
sienzki2.genes.expr_rsem <- as.data.frame(sienzki2.genes.expr_rsem)

# transcripts
sienzki2.tr.expr_rsem <- fetch_rsem_expression(sample_ids = ids, expr_folder = expr_dir, type='isoforms')
fbtrs <- sienzki2.tr.expr_rsem[[1]]$transcript_id

sienzki2.tr.expr_rsem <- sapply(sienzki2.tr.expr_rsem, FUN=function(x) log2(x$TPM+1))
sienzki2.tr.expr_rsem <- data.frame(sienzki2.tr.expr_rsem[,c(3:5)], rep_collapsing(sienzki2.tr.expr_rsem[,c(1:2)]))
rownames(sienzki2.tr.expr_rsem) <- fbtrs
colnames(sienzki2.tr.expr_rsem) <- c('log2.TPM.gfpKD', 'log2.TPM.lucKD', 'log2.TPM.piwiKD', 'log2.TPM.CG9754KD')
sienzki2.tr.expr_rsem <- as.data.frame(sienzki2.tr.expr_rsem)
```

```{r}
save(rozhkov.genes.expr_rsem, rozhkov.tr.expr_rsem, ohtani.genes.expr_rsem, ohtani.tr.expr_rsem, sienzki.genes.expr_rsem, sienzki.tr.expr_rsem, sienzki2.genes.expr_rsem, sienzki2.tr.expr_rsem, file='gene_expression/rsem_expression.Rda')

```

```{r}
unlink('Temp/75_RSEM_cleaned', recursive = T)
```

