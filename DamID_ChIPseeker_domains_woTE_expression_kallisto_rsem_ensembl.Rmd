---
title: "DamID-piwi_gene_expression"
author: "Sergei Ryazansky"
date: "May 18, 2016"
output: html_document
---


Load data and constant variables
```{r}
library(ggplot2)
if (!require("extrafont")) {
  library('extrafont')
}

if (!require("extrafontdb")) {
  library('extrafontdb')
}

source('../Functions/damid_helper_functions.R')

load('gene_expression/kallisto_expr_ensembl.Rda')
load('gene_expression/rsem_expression_ensembl.Rda')
load('Output_data/DamID_domains_TE.Rda')
load('Output_data/DamID_peaks_TE.Rda')

# low-level cutoff for expression
low_cutoff <- 0.1

lev1 <- c("Promoter", "5' UTR", "Exon", 
        "Intron", "3' UTR", "Downstream (<1kb)", "Downstream (1-2kb)",
        "Downstream (2-3kb)", "Distal Intergenic", "w/o domains")

lev2 <- c("Promoter", "5' UTR", "Exon", 
          "Intron", "3' UTR", "w/o domains")

lev3 <- c("Promoter", "Gene body", "w/o domains")

# colors of annotion terms
v <- c('Promoter (<=1kb)'='#2d82af', 'Promoter (1-2kb)'='#a6cee3',
       "5' UTR"='#6f9e4c', "3' UTR"='#f16667', 'Exon'='#fe982c',
       "Intron"='#7d64a5', 'w/o peaks'='#b15928')
```

Preparation of Expression data
```{r}

# genes
## save data as nested list
expr.data.genes <- list(
    'Sienzki et al. 2015' = list(
        'Kallisto' = sienzki2.genes.expr_kallisto2,
        'RSEM' = sienzki2.genes.expr_rsem),
    'Sienzki et al. 2014' = list(
        'Kallisto' = sienzki.genes.expr_kallisto2,
        'RSEM' = sienzki.genes.expr_rsem),
    'Ohtani et al. 2013' = list(
        'Kallisto' = ohtani.genes.expr_kallisto2,
        'RSEM' = ohtani.genes.expr_rsem),
    'Rozhkov et al. 2013' = list(
        'Kallisto' = rozhkov.genes.expr_kallisto2,
        'RSEM' = rozhkov.genes.expr_rsem),
    'Iwasaki et al. 2016' = list(
        'Kallisto' = iwasaki.genes.expr_kallisto2,
        'RSEM' = iwasaki.genes.expr_rsem)
)

## remove outliers: 5% of highly-expressed genes and all <0.1 low-expressed genes
expr.data.genes <- lapply(expr.data.genes, FUN = function(ref){
    lapply(ref, FUN = function(soft){
        soft <- na.omit(soft)
        up_cutoff <- quantile(soft[,1], probs = 0.95)
        soft <- soft[which(soft[,1] <= up_cutoff),]
        soft <- soft[which(rowSums(soft) > low_cutoff),]
    })
})



# transcripts
## save data as nested list
expr.data.trs <- list(
    'Sienzki et al. 2015' = list(
        'Kallisto' = sienzki2.tr.expr_kallisto2,
        'RSEM' = sienzki2.tr.expr_rsem),
    'Sienzki et al. 2014' = list(
        'Kallisto' = sienzki.tr.expr_kallisto2,
        'RSEM' = sienzki.tr.expr_rsem),
    'Ohtani et al. 2013' = list(
        'Kallisto' = ohtani.tr.expr_kallisto2,
        'RSEM' = ohtani.tr.expr_rsem),
    'Rozhkov et al. 2013' = list(
        'Kallisto' = rozhkov.tr.expr_kallisto2,
        'RSEM' = rozhkov.tr.expr_rsem),
    'Iwasaki et al. 2016' = list(
        'Kallisto' = iwasaki.tr.expr_kallisto2,
        'RSEM' = iwasaki.tr.expr_rsem)
)

## remove outliers: 5% of highly-expressed genes and all <0.1 low-expressed genes
expr.data.trs <- lapply(expr.data.trs, FUN = function(ref){
    lapply(ref, FUN = function(soft){
        soft <- na.omit(soft)
        up_cutoff <- quantile(soft[,1], probs = 0.95)
        soft <- soft[which(soft[,1] <= up_cutoff),]
        soft <- soft[which(rowSums(soft) > low_cutoff),]
    })
})

```

Preparation of annotation data
```{r}
# rename 5'UTR 3'UTR exons introns to Gene Body
re_annotation_2 <- function(l) {
    res <- list()
    res[['Promoter']] <- l[['Promoter']]
    res[['Gene body']] <- sort(c(l[["5' UTR"]], l[["3' UTR"]], l[["Exon"]], l[["Intron"]]))
    res[['w/o domains']] <- sort(c(l[['Distal Intergenic']], l[['Downstream (1-2kb)']], l[['Downstream (<1kb)']], l[['Downstream (2-3kb)']]))
    return(res)
}

damid_genes_annot_domains_woTE <- lapply(damid_genes_annot_domains_woTE, re_annotation_2)

damid_trs_annot_domains_woTE <- lapply(damid_trs_annot_domains_woTE, re_annotation_2)

```

Merging of annotation data w/ expression data
```{r}
## Genes

expr.annotated.genes <- data.frame()

for (i in seq(expr.data.genes)) {
    ref_name <- names(expr.data.genes[i])
    expr <- expr.data.genes[[i]]
    result <- data.frame()
    
    for (j in seq(expr)) {
        soft_name = names(expr[j])
        m <- merge_expr_and_annotaion_2(genes.expr = expr[[j]],
                                      ann_list = damid_genes_annot_domains_woTE,
                                      soft = soft_name,
                                      ref = ref_name,
                                      ann = lev3)
        result <- rbind(result, m)
    }
    expr.annotated.genes <- rbind(expr.annotated.genes, result)
}

table(expr.annotated.genes$Class)
#   Promoter   Gene body w/o domains 
#      47961       66739     2713510

## Transcripts
expr.annotated.trs <- data.frame()

for (i in seq(expr.data.trs)) {
    ref_name <- names(expr.data.trs[i])
    expr <- expr.data.trs[[i]]
    result <- data.frame()
    
    for (j in seq(expr)) {
        soft_name = names(expr[j])
        m <- merge_expr_and_annotaion_2(genes.expr = expr[[j]],
                                      ann_list = damid_trs_annot_domains_woTE,
                                      soft = soft_name,
                                      ref = ref_name,
                                      ann = lev3)
        result <- rbind(result, m)
    }
    expr.annotated.trs <- rbind(expr.annotated.trs, result)
}

table(expr.annotated.trs$Class)
#   Promoter   Gene body w/o domains 
#      45620       61436     5012534
```

```{r}
save(expr.annotated.genes, expr.annotated.trs, file = 'Output_data/DamID-domains_annotated_expr_ensembl78.Rda')

```

remove redundant data for w/o domains
```{r}
load('Output_data/DamID-domains_annotated_expr_ensembl78.Rda')

subset_filters <- list(class = c('w/o domains'), pk = c('X.1.48.Inf.'))
w <- data_subsetting(expr.annotated.genes, subset_filters)

subset_filters <- list(class = c('Promoter', 'Gene body'))
nw <- data_subsetting(expr.annotated.genes, subset_filters)

expr.annotated.genes <- rbind(nw, w)
expr.annotated.genes$Class <- ordered(expr.annotated.genes$Class, levels = c('Promoter', 'Gene body', 'w/o domains'))


subset_filters <- list(class = c('w/o domains'), pk = c('X.1.48.Inf.'))
w <- data_subsetting(expr.annotated.trs, subset_filters)

subset_filters <- list(class = c('Promoter', 'Gene body'))
nw <- data_subsetting(expr.annotated.trs, subset_filters)

expr.annotated.trs <- rbind(nw, w)
expr.annotated.trs$Class <- ordered(expr.annotated.trs$Class, levels = c('Promoter', 'Gene body', 'w/o domains'))

```

Plotting all Ref and Soft for Piwi-bind genes
```{r}

e.g <- expr.annotated.genes
e.tr <- expr.annotated.trs

bins <- c('91-100%', '81-90%', '71-80%', '61-70%', '51-60%', '41-50%', '31-40%', '21-30%', '11-20%', '1-10%')
levels(e.g$Peaks) <- bins
e.g$Peaks <- ordered(e.g$Peaks, levels=rev(bins))
levels(e.tr$Peaks) <- bins
e.tr$Peaks <- ordered(e.tr$Peaks, levels=rev(bins))

levels(e.g$Phenotype) <- c('GFP-KD', 'Luc-KD', 'Piwi-KD', 'CG9754-KD', 'EGFP-KD', 'HP1-KD', 'shWhite', 'shPiwi', "log2.TPM.controlKD", "log2.TPM.H1KD")
levels(e.tr$Phenotype) <- c('GFP-KD', 'Luc-KD', 'Piwi-KD', 'CG9754-KD', 'EGFP-KD', 'HP1-KD', 'shWhite', 'shPiwi', "log2.TPM.controlKD", "log2.TPM.H1KD")

# genes

p <- ggplot(aes(y = log2.TPM, x = Class, fill = Peaks), data = e.g, main='Gene expression')
p <- p + geom_boxplot(outlier.shape=NA, position = position_dodge(width=0.8)) + xlab('') + ylab('Gene expression, log2(TPM)')
#p <- p + scale_fill_manual(values=v)
p <- p + theme_bw() +
    theme(axis.text.x = element_text(family="Arial", size=rel(1.3), angle=30, hjust=1), 
          axis.text.y = element_text(family='Arial'),
          axis.title.y = element_text(family='Arial', size=rel(1.3)),
          legend.text = element_text(family='Arial', size=rel(1.1)),
          legend.title = element_text(family='Arial')
          )
#p <- p + ggtitle("Expression of genes with or w/o Piwi")
p <- p + facet_grid(Soft + Reference ~ Phenotype, scales="free_x")
p
#ggsave(file='Plots/Reanalysis_wTE/Expression_genes_all.pdf', height = 6, width = 12)


p <- ggplot(aes(y = log2.TPM, x = Class, fill = Peaks), data = e.tr, main='Gene expression')
p <- p + geom_boxplot(outlier.shape=NA, position = position_dodge(width=0.8)) + xlab('') + ylab('mRNA expression, log2(TPM)')
#p <- p + scale_fill_manual(values=v)
p <- p + theme_bw() +
    theme(axis.text.x = element_text(family="Arial", size=rel(1.3), angle=30, hjust=1), 
          axis.text.y = element_text(family='Arial'),
          axis.title.y = element_text(family='Arial', size=rel(1.3)),
          legend.text = element_text(family='Arial', size=rel(1.1)),
          legend.title = element_text(family='Arial')
          )
#p <- p + ggtitle("Expression of mRNAs with or w/o Piwi")
p <- p + facet_grid(Soft + Reference ~ Phenotype, scales="free_x")
p
#ggsave(file='Plots/Reanalysis_wTE/Expression_transcripts_all.pdf', height = 6, width = 12)

```

colors
```{r}
library(RColorBrewer)
b <- colorRampPalette(c("#2d82af", "white"))
bs <- b(10)
g <- colorRampPalette(c("#6f9e4c", "white"))
gs <- g(10)
cs <- c(rbind(bs, gs))

```


Subset and plotting of data: soft - RSEM, Phenotype-Piwi and Control, Reference -Sienzki2 and Ohtani
```{r}
# filters
subset_filters <- list(soft = 'RSEM',
                       refs = c('Sienzki et al. 2015'),
                       phen = c('log2.TPM.gfpKD', 'log2.TPM.piwiKD'),
                       class = c('Promoter', "Gene body")
                       )

### --> genes
# subsetting
ex.ann.genes.sub <- data_subsetting(expr.annotated.genes, subset_filters)
levels(ex.ann.genes.sub$Phenotype) <- c('GFP-KD', 'Piwi-KD')
levels(ex.ann.genes.sub$Peaks) <- bins
ex.ann.genes.sub$Peaks <- ordered(ex.ann.genes.sub$Peaks, levels=rev(bins))
dim(ex.ann.genes.sub)
#[1] 9004     7

ex.ann.genes.sub$Class <- ordered(ex.ann.genes.sub$Class, levels = c('Promoter', 'Gene body'))

means_df <- data.frame(Phenotype = c("GFP-KD", "Piwi-KD"), M = c(3.2, 3.3))

# plotting of subset
p <- ggplot(aes(y = log2.TPM, x = Class, fill = interaction(Class,Peaks)), data = ex.ann.genes.sub, main='Gene expression')
p <- p + geom_boxplot(outlier.size=0, position = position_dodge(width=0.8)) + xlab('') + ylab('Gene expression, log2(TPM)')
p <- p + scale_fill_manual(values=cs)
p <- p + theme_bw() +
    theme(axis.text.x = element_text(family="Arial", size=rel(1.3)), 
          axis.text.y = element_text(family='Arial'),
          axis.title.y = element_text(family='Arial', size=rel(1.3)),
          legend.text = element_text(family='Arial', size=rel(1.1)),
          legend.title = element_text(family='Arial'),
          legend.position="none"
          )
#p <- p + ggtitle("Expression of genes with or w/o Piwi")
p <- p + facet_grid( ~ Phenotype, scales="free_x")
p <- p + geom_hline(data = means_df, aes(yintercept = M), linetype ="dashed", size = 0.5)
p
#ggsave(file='Plots/Reanalysis_wTE/Expression_genes_subset.pdf', height = 3, width = 6)

### Ohtani
subset_filters <- list(soft = 'RSEM',
                       refs = c('Ohtani et al. 2013'),
                       phen = c('log2.TPM.egfpKD', 'log2.TPM.piwiKD'),
                       class = c('Promoter', "Gene body")
                       )

ex.ann.genes.sub <- data_subsetting(expr.annotated.genes, subset_filters)
levels(ex.ann.genes.sub$Phenotype) <- c('Piwi-KD', 'EGFP-KD')
levels(ex.ann.genes.sub$Peaks) <- bins
ex.ann.genes.sub$Peaks <- ordered(ex.ann.genes.sub$Peaks, levels=rev(bins))
dim(ex.ann.genes.sub)
#[1] 8620      7

ex.ann.genes.sub$Class <- ordered(ex.ann.genes.sub$Class, levels = c('Promoter', 'Gene body'))

means_df <- data.frame(Phenotype = c("Piwi-KD", "EGFP-KD"), M = c(3.27, 3.33))

# plotting of subset
p <- ggplot(aes(y = log2.TPM, x = Class, fill = interaction(Class,Peaks)), data = ex.ann.genes.sub, main='Gene expression')
p <- p + geom_boxplot(outlier.size=0, position = position_dodge(width=0.8)) + xlab('') + ylab('Gene expression, log2(TPM)')
p <- p + scale_fill_manual(values=cs)
p <- p + theme_bw() +
    theme(axis.text.x = element_text(family="Arial", size=rel(1.3)), 
          axis.text.y = element_text(family='Arial'),
          axis.title.y = element_text(family='Arial', size=rel(1.3)),
          legend.text = element_text(family='Arial', size=rel(1.1)),
          legend.title = element_text(family='Arial'),
          legend.position="none"
          )
#p <- p + ggtitle("Expression of genes with or w/o Piwi")
p <- p + facet_grid( ~ Phenotype, scales="free_x")
p <- p + geom_hline(data = means_df, aes(yintercept = M), linetype ="dashed", size = 0.5)
p
ggsave(file='test.pdf', height = 3, width = 6)

### --> mRNAs
# subsetting
ex.ann.trs.sub <- data_subsetting(expr.annotated.trs, subset_filters)
levels(ex.ann.trs.sub$Phenotype) <- c('GFP-KD', 'EGFP-KD', 'Piwi-KD')
levels(ex.ann.trs.sub$Peaks) <- bins
ex.ann.trs.sub$Peaks <- ordered(ex.ann.trs.sub$Peaks, levels=rev(bins))

# plotting of subset
p <- ggplot(aes(y = log2.TPM, x = Class, fill = Peaks), data = ex.ann.trs.sub, main='mRNA expression')
p <- p + geom_boxplot(outlier.size=0, position = position_dodge(width=0.8)) + xlab('') + ylab('mRNA expression, log2(TPM)')
#p <- p + scale_fill_manual(values=v)
p <- p + theme_bw() +
    theme(axis.text.x = element_text(family="Arial", size=rel(1.3)), 
          axis.text.y = element_text(family='Arial'),
          axis.title.y = element_text(family='Arial', size=rel(1.3)),
          legend.text = element_text(family='Arial', size=rel(1.1)),
          legend.title = element_text(family='Arial')
    )
#p <- p + ggtitle("Expression of mRNAs with or w/o Piwi")
p <- p + facet_wrap(~ Reference + Phenotype, scales="free_x")
p
#ggsave(file='Plots/Reanalysis_wTE/Expression_transcripts_subset.pdf', height = 3, width = 6)


### Iwasaki
subset_filters <- list(soft = 'RSEM',
                       refs = c('Iwasaki et al. 2016'),
                       phen = c('log2.TPM.controlKD', 'log2.TPM.piwiKD'),
                       class = c('Promoter', "Gene body")
                       )

ex.ann.genes.sub <- data_subsetting(expr.annotated.genes, subset_filters)
levels(ex.ann.genes.sub$Phenotype) <- c('control-KD', 'Piwi-KD')
levels(ex.ann.genes.sub$Peaks) <- bins
ex.ann.genes.sub$Peaks <- ordered(ex.ann.genes.sub$Peaks, levels=rev(bins))
dim(ex.ann.genes.sub)
#[1] 8690      7

ex.ann.genes.sub$Class <- ordered(ex.ann.genes.sub$Class, levels = c('Promoter', 'Gene body'))

means_df <- data.frame(Phenotype = c("Piwi-KD", "control-KD"), M = c(3.14, 3.26))

# plotting of subset
p <- ggplot(aes(y = log2.TPM, x = Class, fill = interaction(Class,Peaks)), data = ex.ann.genes.sub, main='Gene expression')
p <- p + geom_boxplot(outlier.size=0, position = position_dodge(width=0.8)) + xlab('') + ylab('Gene expression, log2(TPM)')
p <- p + scale_fill_manual(values=cs)
p <- p + theme_bw() +
    theme(axis.text.x = element_text(family="Arial", size=rel(1.3)), 
          axis.text.y = element_text(family='Arial'),
          axis.title.y = element_text(family='Arial', size=rel(1.3)),
          legend.text = element_text(family='Arial', size=rel(1.1)),
          legend.title = element_text(family='Arial'),
          legend.position="none"
          )
#p <- p + ggtitle("Expression of genes with or w/o Piwi")
p <- p + facet_grid( ~ Phenotype, scales="free_x")
p <- p + geom_hline(data = means_df, aes(yintercept = M), linetype ="dashed", size = 0.5)
p
ggsave(file='test.pdf', height = 3, width = 6)
```

mean levels of expression for each group
```{r}
res <- list()
for (s in levels(expr.annotated.genes$Soft)) {
    df1 <- subset(expr.annotated.genes, Soft == s)
    for (r in levels(df1$Reference)) {
        df2 <- subset(df1, Reference == r)
        for (ph in unique(df2$Phenotype)) {
            df3 <- subset(df2, Phenotype == ph)
            df4 <- subset(df3, Class == 'w/o domains')
            m <- mean(df4[,5], na.rm = T)
            print(paste(s,r,ph, m, sep=" "))
#            res[[s]][[r]][[ph]] <- m
        }
    }
}
#[1] "Kallisto Sienzki et al. 2015 log2.TPM.gfpKD 2.71971559767439"
#[1] "Kallisto Sienzki et al. 2015 log2.TPM.lucKD 2.71290755942288"
#[1] "Kallisto Sienzki et al. 2015 log2.TPM.piwiKD 2.73929503574941"
#[1] "Kallisto Sienzki et al. 2015 log2.TPM.CG9754KD 2.71727431913223"
#[1] "Kallisto Sienzki et al. 2014 log2.TPM.gfpKD 2.60164638984195"
#[1] "Kallisto Sienzki et al. 2014 log2.TPM.piwiKD 2.62479839425012"
#[1] "Kallisto Ohtani et al. 2013 log2.TPM.egfpKD 2.90057481262558"
#[1] "Kallisto Ohtani et al. 2013 log2.TPM.piwiKD 2.9179797730809"
#[1] "Kallisto Ohtani et al. 2013 log2.TPM.hp1KD 2.96525629238188"
#[1] "Kallisto Rozhkov et al. 2013 log2.TPM.shWhite 3.17894951104868"
#[1] "Kallisto Rozhkov et al. 2013 log2.TPM.shPiwi 3.09430418044542"
#[1] "Kallisto Iwasaki et al. 2016 log2.TPM.controlKD 2.79890355167081"
#[1] "Kallisto Iwasaki et al. 2016 log2.TPM.piwiKD 2.86784486210101"
#[1] "Kallisto Iwasaki et al. 2016 log2.TPM.H1KD 2.80800186926872"
#[1] "RSEM Sienzki et al. 2015 log2.TPM.gfpKD 3.25686135263577"
#[1] "RSEM Sienzki et al. 2015 log2.TPM.lucKD 3.2852511653585"
#[1] "RSEM Sienzki et al. 2015 log2.TPM.piwiKD 3.23814630453446"
#[1] "RSEM Sienzki et al. 2015 log2.TPM.CG9754KD 3.28923027525354"
#[1] "RSEM Sienzki et al. 2014 log2.TPM.gfpKD 2.55208204539206"
#[1] "RSEM Sienzki et al. 2014 log2.TPM.piwiKD 2.453872314424"
#[1] "RSEM Ohtani et al. 2013 log2.TPM.egfpKD 3.27212292338301"
#[1] "RSEM Ohtani et al. 2013 log2.TPM.piwiKD 3.33377367905154"
#[1] "RSEM Ohtani et al. 2013 log2.TPM.hp1KD 3.43513876593093"
#[1] "RSEM Rozhkov et al. 2013 log2.TPM.shWhite 3.26092915659982"
#[1] "RSEM Rozhkov et al. 2013 log2.TPM.shPiwi 3.58062004743806"
#[1] "RSEM Iwasaki et al. 2016 log2.TPM.controlKD 3.14146455475018"
#[1] "RSEM Iwasaki et al. 2016 log2.TPM.H1KD 3.30508196098096"
#[1] "RSEM Iwasaki et al. 2016 log2.TPM.piwiKD 3.26251013655224"
```



Testing of significance: Promoter, exon, intron vs w/o peaks
```{r}

## genes
pvals_genes <- vector('list', length=5)
names(pvals_genes) <- names(expr.data.genes)

for (i in seq(pvals_genes)) {
    ref_name <- names(pvals_genes[i])
    
    for (soft_name in c('Kallisto', 'RSEM')) {
        data <- data_subsetting(expr.annotated.genes, 
                                list(soft=soft_name,
                                     refs=ref_name))
        pvs <- list()
        for (ph in unique(data$Phenotype)) {
            res <- data_subsetting(data, list(phen=c(ph)))
            
            pvsr <- list()
            for (bin in unique(data$Peaks)) {
                resf <- data_subsetting(res, list(pk=c(bin), class=c('Promoter', 'Gene body')))
                df_c <- data_subsetting(res, list(class=c('w/o domains')))
                pvsr[[bin]] <- pvalues_wo_domains(resf, df_control=df_c)
            }
            
            pvs[[ph]] <- pvsr
        }
        pvals_genes[[ref_name]][[soft_name]] <- pvs
    }
}

## mRNAs
pvals_trs <- vector('list', length=5)
names(pvals_trs) <- names(expr.data.trs)

for (i in seq(pvals_trs)) {
    ref_name <- names(pvals_trs[i])
    
    for (soft_name in c('Kallisto', 'RSEM')) {
        data <- data_subsetting(expr.annotated.trs, 
                                list(soft=soft_name,
                                     refs=ref_name))
        pvs <- list()
        for (ph in unique(data$Phenotype)) {
            res <- data_subsetting(data, list(phen=c(ph)))
            
            pvsr <- list()
            for (bin in unique(data$Peaks)) {
                resf <- data_subsetting(res, list(pk=c(bin), class=c('Promoter', 'Gene body')))
                df_c <- data_subsetting(res, list(class=c('w/o domains')))
                pvsr[[bin]] <- pvalues_wo_domains(resf, df_control=df_c)
            }
            
            pvs[[ph]] <- pvsr
        }
        pvals_trs[[ref_name]][[soft_name]] <- pvs
    }
}

```



- associated with TEs?